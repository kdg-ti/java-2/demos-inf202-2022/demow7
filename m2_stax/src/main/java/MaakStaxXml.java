import be.kdg.model.Piloot;
import be.kdg.model.Piloten;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

public class MaakStaxXml {
    private static final String FILENAME = "data/piloten.xml";

    public static void main(String[] args) {
        List<Piloot> piloten = Piloten.getPiloten();

        try {
            PrintWriter printWriter = new PrintWriter(
                    new OutputStreamWriter(new FileOutputStream(FILENAME)));

            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter streamWriter = xmlOutputFactory.createXMLStreamWriter(printWriter);
            streamWriter = new IndentingXMLStreamWriter(streamWriter);

            //TODO: document en rootelement wegschrijven:

            for (Piloot piloot : piloten) {
                //TODO: alle Piloot-elementen met attribuut en children wegschrijven

            }
            //TODO: end-elementen wegschrijven

            streamWriter.flush();
            streamWriter.close();
            printWriter.close();
            System.out.println("XML bestand: \"" + FILENAME + "\" opgeslagen!");

        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }
}