package be.kdg.model;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Piloten {
    private static final List<Piloot> piloten = Arrays.asList(
            new Piloot("Mercedes", "Hamilton", 44, LocalDate.of(2018, 10, 12)),
            new Piloot("Mercedes", "Rosberg", 6, LocalDate.of(2017, 4, 1)),
            new Piloot("Ferrari", "Vettel", 5, LocalDate.of(2018, 7, 27)),
            new Piloot("Ferrari", "Räikkönen", 7, LocalDate.now()),
            new Piloot("McLaren", "Bottas", 77, LocalDate.of(2018, 9, 5)),
            new Piloot("McLaren", "Massa", 19, LocalDate.now()),
            new Piloot("Red Bull", "Ricciarddo", 3, LocalDate.of(2016, 11, 30)),
            new Piloot("Red Bull", "Kvyat", 26, LocalDate.now()));

    public static List<Piloot> getPiloten() {
        return piloten;
    }
}
